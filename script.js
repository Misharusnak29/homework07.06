/* Теоретичні питання 
1. 
function myFunction()
const myFunction = function()
const myFunction = () =>

myFunction();

2.Оператор return використовується для того, щоб повернути значення з функції і завершити її виконання. Після виконання оператора return функція припиняє своє виконання, і будь-який код після нього не буде виконано.

function add(a, b) {
  return a + b;
}
let result = add(2, 3);

3.Параметри — це змінні, які оголошуються в сигнатурі функції і отримують значення під час виклику функції.
Аргументи — це значення, які передаються функції під час її виклику.
4.function greet(name) {
  console.log("Hello, " + name + "!");
}

function processUserInput(callback) {
  let name = prompt("Please enter your name.");
  callback(name);
}

processUserInput(greet);

*/

// Практичні завдання
//1.

// function divide(a, b) {
//     if (b === 0) {
//       return "На 0 ділити не можна";
//     }
//     return a / b;
//   }
  
//   console.log(divide(10, 2));
//   console.log(divide(10, 0));
  
// let number1

// while (true) {
//     let input1 = prompt("Введіть перше число:");
//     number1 = parseFloat(input1);
//     if (!isNaN(number1)) {
//       break;
//     }
//     alert("Будь ласка, введіть правильне число.");
//   }

//   let number2;
//   while (true) {
//     let input2 = prompt("Введіть друге число:");
//     number2 = parseFloat(input2);
//     if (!isNaN(number2)) {
//       break;
//     }
//     alert("Будь ласка, введіть правильне число.");
//   }



//     const operation = prompt("Введіть операцію + , - , * , / ");

//   function calculate(number1, number2, operation) {
//     switch (operation) {
//       case "+":
//         return number1 + number2;
//       case "-":
//         return number1 - number2;
//       case "*":
//         return number1 * number2;
//       case "/":
//         return number2 !== 0 ? number1 / number2 : "Ділити на нуль не можна!";
//       default:
//         return "Такої операції не існує";
//     }
//   }

// const result = calculate(number1, number2, operation);
// alert(result)

// 3.

// function factorial(n) {
//     if (n < 0) return "Факторіал для від’ємних чисел не визначений";
//     if (n === 0 || n === 1) return 1;
//     let result = 1;
//     for (let i = 2; i <= n; i++) {
//       result *= i;
//     }
//     return result;
//   }
  
//   const num = getNumber("Введіть число, щоб обчислити факторіал:");
//   console.log(`Факторіал від ${num} це ${factorial(num)}`);
  
